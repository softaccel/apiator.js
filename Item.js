/**
 *
 * @param options
 * @returns {{relationships: null, view: null, attributes: null, id: null, collection: null, type: null, url: null}}
 */
function Item(options) {
    let _item = {
        id: null,
        type: null,
        attributes: null,
        relationships: null,
        view: null,
        collection: null,
        url: null,
    };

    let eventTypes = [
        'beforeLoad','loadOk','loadFail','loadAlways',
        'beforeCreate','createOk','createFail','createAlways',
        'beforeUpdate','updateOk','updateFail','updateAlways',
        'beforeDelete','deleteOk','deleteFail','deleteAlways'];
    _item = Object.assign(_item,EventsEmitter(eventTypes));

    /**
     *
     * @param {{}} item Item object to fill with data
     * @param {{}} itemData
     * @param {{}} doc
     * @todo verify recursion .....
     */
    function parseData(item,itemData,doc) {
        // extract all resource objects into an array (if not done already)
        flattenDoc(doc);

        if(!itemData.hasOwnProperty("data") || !isValidObject(itemData.data)) {
            throw "Invalid resource";
        }

        // retrieve self URL
        if(itemData.hasOwnProperty("links") && itemData.links.hasOwnProperty("self")) {
            item.setUrl(itemData.links.self);
        }

        // no relationships => job done & return
        if(!item.hasOwnProperty("relationships") || item.relationships.constructor!==Object)
            return item.set(itemData.data);

        // iterate relationships data and create Item Objects
        item.relationships.getOwnPropertyNames(function (rel_name) {
            let relUrl;
            if(item.relationships[rel_name].hasOwnProperty("links")
                && item.relationships[rel_name].links.hasOwnProperty("related"))
                relUrl = item.relationships[rel_name].links.related;

            // flatten data
            if(item.relationships[rel_name].hasOwnProperty("data"))
                item.relationships[rel_name] = item.relationships[rel_name].data ;

            // empty 1:1 relation
            if(item.relationships[rel_name]===null)
                return;

            // 1:1 relation
            if(item.relationships[rel_name].constructor===Object) {
                item.relationships[rel_name] = Item({
                    url: relUrl,
                    data: parseData(item.relationships[rel_name],doc)
                });
            }

            // 1:n relationship
            if(item.relationships[rel_name].constructor===Array)
                item.relationships[rel_name] = item.relationships[rel_name].map(function (rel_item_data) {
                    return Item({
                        url: relUrl,
                        data: parseData(rel_item_data,doc)
                    });
                });
        });

        return item;
    }

    // check if initializing data is provided and if yes extract it and delete the member from options
    let data;
    if(options.hasOwnProperty("data")) {
        data = options.data;
        delete options.data;
    }

    Object.assign(_item,parseOptions(options));
    let storage = options.hasOwnProperty("storage")?options.storage:new Storage();

    if(_item.view)
        _item.view.item = _item;

    /**
     * set url; if url is a string, it will parse it into an URL Object
     * @param url
     */
    _item.setUrl = function (url) {
        if(!this.url && !url && this.collection && this.collection.url && this.id!==null) {
            this.url = Object.assign({}, this.collection.url);
            this.url.path += "/" + this.id;
        }

        if(typeof url==="string")
            this.url = URL(url);
        if(typeof url==="object")
            this.url = url;
        return this;
    };

    /**
     * load data from remote storage. Trigger an AJAX call
     */
    _item.loadFromRemote = function () {
        storage.read(this.url,{},this.loadFromData,this.fail, this);
    };

    /**
     * set attributes (single or bulk)
     * @param {{}} attributes
     * @param newValue
     * @returns {_item}
     */
    _item.setAttr = function(attributes,newValue) {
        if(typeof attributes==="string") {
            setAttr(attributes,newValue);
            return this;
        }

        if(typeof attributes==="object" && attributes.constructor===Object){
            Object.getOwnPropertyNames(attributes).forEach(function (attrName) {
                setAttr(attrName,attributes[attrName]);
            });
        }
    };

    _item.setId = function(id) {
        this.id = id;
    };

    _item.setType = function(type) {
        this.type = type;
    };

    _item.setRelationship = function() {


    };

    function setAttr(attrName,value) {
        let oldVal;
        if(_item.attributes.hasOwnProperty(attrName))
            oldVal = this.attributes[attrName];

        let event = {
            type: "attributeUpdate",
            source: _item,
            oldVal: oldVal,
            newVal: value
        };

        _item.attributes[attrName] = value;
        _item.dispatch("attributeUpdate",event);
    }

    _item.set = function(data) {
        if(typeof data!=="object" && data.constructor!==Object)
            throw "Invalid item data";
        if(data.hasOwnProperty("id"))
            this.setId(data.id);
        if(data.hasOwnProperty("type"))
            this.setType(data.type);
        if(data.hasOwnProperty("attributes"))
            this.setAttr(data.attributes);
        if(data.hasOwnProperty("relationships")) {
            data.relationships.getOwnPropertyNames.forEach(function (rel_name) {
                this.setRelationship = "";
            }, this);
        }
    };


    _item.setUrl(_item.url);

    if(typeof data!=="undefined") {
        setData(data,null,null,_item);
    }

    function setData(data,text,xhr,ctx) {
        let obj = _item;

        if(typeof ctx==="object" && ctx.hasOwnProperty("collection"))
            obj = ctx;

        Object.assign(obj,obj.parseData(data,data));

        if(!data.hasOwnProperty("forceLoad") || !data.forceLoad) {
            if(obj.view)
                obj.view.render();
        }
        obj.setUrl();
        return this;
    };

    /**
     * loads data statically from data parameter
     * @param data
     * @param text
     * @param xhr
     * @param ctx
     * @returns {_item}
     */
    _item.loadFromData = function (data,text,xhr,ctx) {
        let obj = _item;

        if(typeof ctx==="object" && ctx.hasOwnProperty("collection"))
            obj = ctx;

        Object.assign(obj,obj.parseData(data,data));

        if(!data.hasOwnProperty("forceLoad") || !data.forceLoad) {
            if(obj.view)
                obj.view.render();
        }
        obj.setUrl();
        return this;
    };

    /**
     * create a record
     */
    // _item.createOnRemote = function (data) {
    //     storage.create(this.url,{},data,this.loadFromData,this.fail,this)
    //     //function (location, opts, data, onSuccess, onFail,ctx)
    // };

    /**
     *
     * @param xhr
     * @param statusText
     * @param error
     */
    _item.fail = function (xhr,statusText,error) {
        console.log(xhr,statusText,error);
    };


    /**
     * @final
     */
    _item.toJSON = function () {
        let json = {
            type: this.type,
            attributes: this.attributes
        };
        if (this.id)
            json.id = this.id;
        if (this.attributes)
            json.attributes = this.attributes;

        if (!this.hasOwnProperty("relationships"))
            return json;

        for (let relName in this.relationships) {
            if (!this.relationships.hasOwnProperty(relName))
                continue;

            json.relationships[relName] = {
                data: null
            };
            if (this.relationships[relName] === null)
                continue;

            // 1:1 relation
            if (this.relationships[relName].constructor === Object) {
                json.relationships[relName].data = this.relationships[relName].toJSON();
                continue;
            }

            // invalid relation data (not null, not an object, not an array)
            if (this.relationships[relName].constructor !== Array) {
                delete this.relationships[relName];
                delete json.relationships[relName];
                continue;
            }

            // 1:n relations
            json.relationships[relName].data = [];
            for (let i = 0; i < this.relationships[relName].length; i++)
                json.relationships[relName].data.push(this.relationships[relName][i].toJSON());
        }
        return json;
    };

    /**
     *
     */
    _item.update = function (itemData) {
        // console.log(itemData,itmObj,itmObj.attributes);
        let toUpdate = {
            id: this.id,
            type: this.type,
            attributes: {},
            relationships: {}
        };

        let attrsToUpdate = {};
        let relsToUpdate = {};

        // check attributes
        Object.getOwnPropertyNames(this.attributes).forEach(function (attrName) {
            if(itemData.hasOwnProperty(attrName) && itemData[attrName]!==this.attributes[attrName]) {
                toUpdate.attributes[attrName] = itemData[attrName];
            }
        }, this);

        // check relationships
        Object.getOwnPropertyNames(this.relationships).forEach(function (relName) {
            if(!itemData.hasOwnProperty(relName))
                return;
            if(this.relationships[relName]==null || this.relationships[relName].id !== itemData[relName]) {
                toUpdate.relationships[relName] = {
                    id: itemData[relName]
                };
                if(this.relationships[relName] && this.relationships[relName].hasOwnProperty("type"))
                    toUpdate.relationships[relName].type = this.relationships[relName].type;

                console.log("update " + relName);
            }
        }, this);

        if(Object.getOwnPropertyNames(toUpdate.attributes).length
            || Object.getOwnPropertyNames(toUpdate.relationships).length)

            storage.update(this.url, {}, JSON.stringify({data: toUpdate}),
                function (data,txt,xhr,ctx) {
                    let newData = ctx.parseData(data,data);
                    Object.assign(ctx,newData);
                    ctx.view.render();
                }, function (xhr,txt,err,ctx) {

                }, this);


    };

    /**
     * delete item
     */
    _item.delete = function (cb) {
        storage.remove(this.url,{},null,onRemoveComplete,onRemoveFail, onRemoveAlways, this);
    };

    /**
     *
     * @param data
     * @param textStatus
     * @param jqXHR
     * @param ctx
     */
    function onRemoveComplete (data,textStatus, jqXHR, ctx) {
        if(ctx.view)
            ctx.view.remove();
        if(typeof cb!=="undefined" && cb.constructor === Function)
            cb();
        delete _item;
    }

    /**
     *
     * @param xhr
     * @param statusText
     * @param error
     */
    function onRemoveFail(xhr,statusText,error) {
        _item.fail(xhr,statusText,error);
    }

    function onRemoveAlways(jqXHR, textStatus) {

    }

    return _item;
}