/**
 *
 * @param options
 * @returns {{container: null, el: null, collection: null, itemsContainer: null}}
 */
function CollectionView(options) {
    let _collectionView = {
        el: null,
        container: null,
        collection: null,
        itemsContainer: null,
        allowempty: true
    };
    Object.assign(_collectionView,parseOptions(options));

    /**
     *
     */
    _collectionView.reset = function () {
        if(this.allowempty===true)
            _collectionView.itemsContainer.empty();
        return _collectionView;
    };

    /**
     *
     */
    _collectionView.render = function () {
        // console.log("render Collection View",this);
        if(this.collection.navtype==="page")
            this.reset();
        this.collection.items.forEach(function (item) {
            this.append(item.view.render(true));
        },this);
        return this;
    };

    _collectionView.append = function(el) {
        this.itemsContainer.append(el)
    };

    return _collectionView;

}