Create CRUD dynamic views from a REST API. 
 
Apiator.JS is a JavaScript library to use for rendering dynamic views
using a REST API. You can display single data objects or data collections. 