/**
 *
 * @param opts
 * @returns {{template: null, view: null, total: null, offset: number, navtype: string, pageSize: number, paging: null, url: null}}
 */
function Collection(opts) {
    let _collection = {
        url: null,
        paging:null,
        view: null,
        offset: 0,
        total: null,
        pageSize: 10,
        template: null,
        navtype: "page",
        type: null,
    };
    opts = parseOptions(opts);

    let eventTypes = [
        'beforeLoad','loadOk','loadFail','loadAlways',
        'beforeCreate','createOk','createFail','createAlways',
        'beforeUpdate','updateOk','updateFail','updateAlways',
        'beforeDelete','deleteOk','deleteFail','deleteAlways'];
    _collection = Object.assign(_item,EventsEmitter(eventTypes));

    let listeners = {
        dataRequest: [],
        dataReceived: [],
        responseEnd: [],
        dataLoaded: []
    };

    function dispatchEvent(type,ev) {
        listeners[type].forEach(function (listener) {
            listener.cb(ev,listener.ctx);
        });
    }

    /**
     *
     * @param type
     * @param cb
     * @param ctx
     * @returns {_collection}
     */
    _collection.on = function(type,cb,ctx) {
        if(typeof type === "undefined")
            throw "Event type not specified";
        if(typeof  cb === "undefined" || cb.constructor !== Function)
            throw "Invalid callback function for event "+type;
        if(!listeners.hasOwnProperty(type))
            return;
        listeners[type].push({cb:cb,ctx: typeof ctx!=="undefined"?ctx:_collection});
        return this;
    };

    /**
     *
     * @param type
     * @returns {_collection}
     */
    _collection.off = function(type) {
        if(typeof type === "undefined")
            throw "Event type not specified";
        if(!listeners.hasOwnProperty(type))
            return;
        listeners[type] = [];
        return this;
    };
    _collection.setUrl = function(url) {
        this.url = URL(url);
        return this;
    };

    Object.assign(_collection,opts);

    _collection.setUrl(_collection.url);

    if(_collection.view)
        _collection.view.collection = _collection;
    if(_collection.url && _collection.url.parameters && _collection.url.parameters.hasOwnProperty("page[offset]"))
        _collection.offset =  self.url.parameters["page[offset]"]*1;
    if(_collection.url && _collection.url.parameters && _collection.url.parameters.hasOwnProperty("page[limit]"))
        _collection.pageSize =  _collection.url.parameters["page[limit]"]*1;
    if(_collection.total)
        _collection.total = _collection.total*1;

    if(["page","scroll"].indexOf(_collection.navtype)===-1)
        throw "Invalid navigations type. Should be page or scroll";

    let storage = opts.hasOwnProperty("storage") ? opts.storage : (
        opts.hasOwnProperty("ajaxOpts") ? new Storage(opts.ajaxOpts) : new Storage()
    );

    /**
     *
     * @param data
     * @param statusTxt
     * @param xhr
     * @param ctx
     */
    function receiveRemoteData(data,statusTxt,xhr,ctx) {
        let obj = _collection;

        if(ctx && ctx.hasOwnProperty("navtype"))
            obj = ctx;

        data = parse(data);

        // dispatch dataReceived event
        obj.dispatch("loadOK",{type: "loadOK",source: obj,recvData: data});

        if(data==null || data===undefined)
            return;

        // received data is a collection
        if(data.constructor===Array) {
            obj.loadFromData(data);
            obj.view.render();
            if (obj.paging)
                obj.paging.render();
        }

        // received data is an item => add it
        if(data.constructor===Object) {
            obj.addItem(data);
            obj.view.render();
        }
    }

    /**
     *
     * @param xhr
     * @param txt
     * @param err
     * @param ctx
     */
    function receiveRemoteDataFail(xhr, txt, err,ctx) {
        console.log(xhr, txt, err,ctx);
    }

    /**
     *
     */
    function receiveRemoteDataAlways() {

    }

    /**
     *
     * @param data
     * @param statusTxt
     * @param xhr
     * @param ctx
     */
    _collection.loadFromData = function (data) {
        let obj = this;

        if (data.constructor !== Array)
            throw "Invalid data type received. Should be an array.";

        if(obj.navtype==="page")
            obj.items = [];

        data.forEach(function (item) {
            obj.addItem(item);
        });
        // dispatch dataReceived event
        dispatchEvent("dataLoaded",{type: "dataLoaded",source: obj});
    };

    /**
     *
     */
    _collection.loadFromRemote = function () {
        console.log("Load from remote "+this.url);
        storage.read(this.url, {}, receiveRemoteData, receiveRemoteDataFail, receiveRemoteDataAlways, this);
    };

    /**
     * send request to add new item to storage
     * @param itemData
     */
    _collection.reqAddItem = function(itemData) {
        let data = {
            data: {
                type: this.type,
                attributes:  itemData
            }
        };
        console.log("Trigger item creation",this,itemData);
        storage.create(this.url,{contentType:"application/vnd.api+json"},JSON.stringify(data),receiveRemoteData,receiveRemoteDataFail,this);
    };


    _collection.addItem = function (itemData,txtStatus,xhr,ctx) {
        let obj = this;

        let opts = {
            type: obj.type,
            collection: obj,
            view: ItemView({
                template: obj.template,
                container: obj.view
            })
        };

        if(itemData.id) {
            opts.url = Object.assign({},obj.url);
            opts.url.path += "/" + itemData.id;
        }


        let newItem = Item(opts);
        obj.items.push(newItem.loadFromData(itemData));
        //console.log(newItem,a++);
        return newItem;
    };


    /**
     *
     * @param data
     */
    function parse(data) {
        flattenDoc(data);
        if (!data.hasOwnProperty("data"))
            return data;


        if (data.hasOwnProperty("meta")) {
            if (data.meta.hasOwnProperty("totalRecords"))
                _collection.total = data.meta.totalRecords*1;
            if (data.meta.hasOwnProperty("offset"))
                _collection.offset = data.meta.offset;

        }
        return data.data;
    }

    return _collection;
}