/**
 *
 * @param options
 * @returns {{}|*}
 */
function parseOptions(options) {
    if(typeof options==="undefined")
        return {};
    if(options.constructor===Object)
        return options;
    throw ["Invalid options"];
}

/**
 *
 * @param doc
 */
function flattenDoc(doc) {
    let arr = [];
    if(doc.hasOwnProperty("data") && doc.data!==null) {
        if(doc.data.constructor===Array)
            arr = doc.data;
        else
            arr.push(doc.data)
    }
    if(doc.hasOwnProperty("includes"))
        arr = arr.concat(doc.includes);

    arr.forEach(function (item) {
        if(!itemsArr.hasOwnProperty(item.type+"/"+item.id))
            itemsArr[item.type+"/"+item.id] = item;
    });
}

/**
 *
 * @param events
 * @returns {{events: {}}}
 * @constructor
 */
function EventsEmitter(events) {
    if(typeof events!=="object" )
        throw "Invalid Events Emiter initializer";

    let ee = {
        events: {}
    };

    if(events.constructor===Array)
        events.forEach(function(eventName){
            ee.events[eventName] = [];
        });
    else
        Object.getOwnPropertyNames(events).forEach(function(eventName){
            ee.events[eventName] = events[eventName];
        });

    /**
     *
     * @param eventName
     * @param event
     */
    ee.dispatch = function (eventName,event) {
        if(!ee.hasOwnProperty(eventName))
            return;
        event.type = eventName;
        ee.events[eventName].forEach(function (listener) {
            (async function(listener) {
                listener(event);
            })(listener);
        });
    };

    /**
     *
     * @param eventName
     * @param listener
     */
    ee.on = function (eventName,listener) {
        if(!ee.hasOwnProperty(eventName))
            return;
        ee.events[eventName].push(listener);
    };

    /**
     *
     * @param eventName
     */
    ee.off = function (eventName) {
        if(!ee.hasOwnProperty(eventName))
            return;
        ee.events[eventName] = [];
    };

    return ee;
}


// function HooksHost() {
//
// }

function isValidObject(obj) {
    return typeof obj==="object" && obj.constructor!==Object;
}

function isValidArray(obj) {
    return typeof obj==="object" && obj.constructor!==Object;
}

function isValidString(obj) {
    return typeof obj==="string";
}







/**
 * URL object factory method
 * @param url
 * @returns {{path: *, protocol: *, fragment: *, fqdn: *, port: *, toString: (function(): string), parameters: *}}
 */
function URL(url) {

    let regExp = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
    let parts = regExp.exec(url);



    let urlObj = {
        protocol: parts[1],
        fqdn: parts[3],
        port: parts[4],
        path: parts[5],
        parameters: parts[6]?parts[6]:"",
        fragment: parts[7],
        toString: function () {
            let str = "";
            if(this.protocol && this.fqdn)
                str += this.protocol+"://"+this.fqdn;
            if(this.port)
                str += this.port;
            if(this.fqdn)
                str +=  "/";
            if(this.path)
                str += this.path;
            if(this.parameters) {
                str += "?" + this.parameters.toString();
            }
            if(this.fragment)
                str += "#"+this.fragment;
            return str;
        }
    };


    if(parts[1]===undefined) {
        urlObj.path = parts[2]+parts[3]+"/"+parts[5];
        urlObj.protocol = null;
        urlObj.fqdn = null;
        urlObj.port = null;
    }

    if(urlObj.parameters) {
        let tmp = urlObj.parameters.split("&");
        urlObj.parameters = {};
        tmp.forEach(function (item) {
            if(!item || item==="")
                return;
            let eqPos = item.indexOf("=");
            if(eqPos===-1)
                urlObj.parameters[item] = "";
            urlObj.parameters[item.substr(0,eqPos)] = item.substr(eqPos+1);
        });
        urlObj.parameters.toString = function () {
            let paras = [];
            for(let para in this) {
                if(this.hasOwnProperty(para) && para!=="toString")
                    paras.push(para+"="+this[para]);
            }
            return paras.join("&");
        };
    }
    return urlObj;
}