/**
 *
 * @param options
 * @returns {{template: null, container: null, item: null, el: null}}
 */
function ItemView(options) {
    let itmView = {
        template: null,
        container: null,
        item: null,
        el: null,
        id: uid()
    };
    options = parseOptions(options);
    Object.assign(itmView,options);
    if (!itmView.template)
        throw "Invalid ItemView template";

    /**
     *
     */
    itmView.render = function (returnView) {
        // console.log("render Item View",this);
        //throw "Test";
        let view = $(this.template(this.item));
        view.data("item",this.item).attr("data-type","item").attr("id","");
        if(this.el && this.el.parents().length) {
            this.el.replaceWith(view);
        }
        else if(this.container  && !returnView) {
            //this.container.append(view);
        }
        this.el = view;
        this.el.data("instance",this.item);
        this.el.attr("id",this.id);
        this.el.find("[data-action="+EDIT_BUTTON_ACTION+"]").attr("data-instance","#"+this.id);
        return this.el;
    };

    itmView.remove = function () {
        this.el.fadeOut({
            complete: ()=>{itmView.el.remove();}
        });
    };
    return itmView;
}